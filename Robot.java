/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

/*
 * Rewritten 11.0 (Power UP) code for Rumble 2018.
 * [F] for Godot.
 * "Programming did not give up"
 * Code rewrite took place on October 21.
 */

//TODO: Compensate for pull

package org.usfirst.frc.team540.robot;

import java.lang.invoke.SwitchPoint;

import com.mindsensors.CANSD540;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Robot extends IterativeRobot {
	final String defaultAuto = "Default";
	// timer modes
	final String baselineTime = "Baseline with timer";
	final String switchMiddleEnc = "Switch with encoder (M)";
	final String switchRightEnc = "Switch with encoder(R)";
	final String switchLeftEnc = "Switch with encoder (L)";

	String autoSelected;
	SendableChooser<String> chooser = new SendableChooser<>();

	// motor, joy-stick, and controller fields
	CANSD540 frontLeft, frontRight, backLeft, backRight, midLeft, midRight, intake1, intake2, intakeVert, hook, winch;

	ADXRS450_Gyro gyro;
	Joystick leftJoy, rightJoy;
	XboxController xbox;

	// Sensor fields
	Encoder enc1, enc2;
	AnalogInput IR;

	// will be used in driveCode() to get current movement
	double left, right, intakeL, intakeR, hooker, wench;

	// sensor fields
	double angle, irDist, pulse, dist;
	// double liftPulse;

	// variable to compensate for inconsistent motor powers
	double scale;

	boolean height, toggle, cube, still;

	// Constant multiplied by the
	final static double TURN_CONSTANT = 0.5;

	// FMS
	String FMS, enemyFMS;

	final static double DRIVE_CONSTANT = -0.8;

	// Counter and side for Auto and reverse for tele-op
	int counter, reverse, side;

	@Override
	public void robotInit() {
		// Default currently turns
		chooser.addObject("Baseline with timer", baselineTime);
		chooser.addObject("Left starting position", switchLeftEnc);
		chooser.addObject("Right starting position", switchRightEnc);
		chooser.addObject("Middle starting position", switchMiddleEnc);
		chooser.addDefault("Default (Do Nothing)", defaultAuto);

		SmartDashboard.putData("Auto choices", chooser);

		// wheel motors
		frontLeft = new CANSD540(6);
		backLeft = new CANSD540(7);
		frontRight = new CANSD540(4);
		backRight = new CANSD540(5);

		// intake motors
		intake1 = new CANSD540(11);
		intake2 = new CANSD540(9);
		intakeVert = new CANSD540(10);

		// climber and winch
		hook = new CANSD540(2);
		winch = new CANSD540(1);

		// joystick and controller motors
		leftJoy = new Joystick(0);
		rightJoy = new Joystick(1);
		xbox = new XboxController(2);

		// drivetrain encoder
		enc1 = new Encoder(2, 3, false);
		enc1.setMaxPeriod(0.1);
		enc1.setMinRate(5);
		// 0.00078487 ft per pulse -> 10 ft per 12741 pulses
		enc1.setDistancePerPulse(0.00078487);
		enc1.setSamplesToAverage(10);

		// sensors
		gyro = new ADXRS450_Gyro();
		IR = new AnalogInput(0);

		// Note: Never use gyro.calibrate() and gyro.reset() together
		// calibrates encoders
		enc1.reset();

		// initializes counter
		counter = 0;

		// initializes reverse
		reverse = 1;

		// initializes toggle
		toggle = false;

		// initializes scale
		scale = 0.75;

		// initialize voltage ramps
		frontLeft.setVoltageRamp(100);
		backLeft.setVoltageRamp(100);
		frontRight.setVoltageRamp(100);
		backRight.setVoltageRamp(100);

		intake1.setVoltageRamp(100);
		intake2.setVoltageRamp(100);
		intakeVert.setVoltageRamp(100);

		// Camera Server
		CameraServer server = CameraServer.getInstance();
		UsbCamera cam = server.startAutomaticCapture();
		cam.setResolution(320, 240);
		cam.setFPS(15);

		// FMS Initialization
		FMS = "";

	}

	public void autonomousInit() {
		autoSelected = chooser.getSelected();
		System.out.println("Auto selected: " + autoSelected);

		// FMS
		while (FMS.length() < 1) {
			FMS = DriverStation.getInstance().getGameSpecificMessage();
		}
		SmartDashboard.putString("Our Switch Side: ", FMS);

		counter = 0;
		gyro.reset();
		enc1.reset();
	}

	@Override
	public void autonomousPeriodic() {
		// Gets sensor values and displays them in SmartDashboard

		angle = gyro.getAngle();
		irDist = IR.getVoltage();
		pulse = enc1.get();
		dist = enc1.getDistance();

		SmartDashboard.putNumber("Angle: ", angle);
		SmartDashboard.putNumber("IR Distance: ", irDist);
		SmartDashboard.putNumber("Pulse Count: ", pulse);
		SmartDashboard.putNumber("Distance Traveled: ", dist);
		SmartDashboard.putNumber("Auto Counter: ", counter);

		switch (autoSelected) {
		// MOTORS ARE BOTH INVERTED IN AUTO - I.E. negative is forward

		case switchLeftEnc:
			if (FMS.charAt(0) == 'L') {
				// Forwards 14 feet
				if (counter == 0) {
					moveForward(13);
				}

				// Turn 90 degrees right
				if (counter == 1) {
					turnRight(90);
				}

				// Raise the intake
				if (counter == 2) {
					raiseIntake();
				}

				// Move up to the switch
				if (counter == 3) {
					moveForward(2);
				}

				// Shoot the cube
				if (counter == 4) {
					shootCube();
				}

				if (counter == 5) {
					killMotors();
				}

			} else {
				// Forwards 14 feet
				// Also runs if there's an error with FMS
				if (counter == 0) {
					moveForward(14);
				}

				// Kill the motors
				if (counter == 1) {
					killMotors();
				}

			}
			break;

		case switchRightEnc:
			if (FMS.charAt(0) == 'R') {
				// Forwards 14 feet
				if (counter == 0) {
					moveForward(13);
				}

				// Turn 90 degrees left
				if (counter == 1) {
					turnLeft(90);
				}

				// Raise the intake
				if (counter == 2) {
					raiseIntake();
				}

				// Move up to the switch
				if (counter == 3) {
					moveForward(2);
				}

				// Shoot the cube
				if (counter == 4) {
					shootCube();
				}

				if (counter == 5) {
					killMotors();
				}

			} else {
				// Forwards 14 feet
				// Also runs if there's an error with FMS
				if (counter == 0) {
					moveForward(14);
				}

				// Kill the motors
				if (counter == 1) {
					killMotors();
				}

			}
			break;

		case switchMiddleEnc:
			if (FMS.charAt(0) == 'L') {
				// Forwards two feet to turn
				if (counter == 0) {
					moveForward(2);
					System.out.println("0");
				}

				// Turn 45 degrees left
				if (counter == 1) {
					turnLeft(45);
					System.out.println("1");
				}

				// Forwards 5 feet
				if (counter == 2) {
					moveForward(5);

					System.out.println("2");
				}

				// Turn 45 degrees right (to face the switch)
				if (counter == 3) {
					turnRight(45);
				}

				// Raise intake
				if (counter == 4) {
					raiseIntake();
				}

				// Forwards 2 feet
				if (counter == 5) {
					moveForward(2);
				}

				// Shoot cube
				if (counter == 6) {
					shootCube();
				}

				// Kill motors
				if (counter == 7) {
					killMotors();
				}
			} else if (FMS.charAt(0) == 'R') {
				// Forwards two feet to turn
				if (counter == 0) {
					moveForward(2);
				}

				// Turn 45 degrees right
				if (counter == 1) {
					turnRight(45);
				}

				// Forwards 5 feet
				if (counter == 2) {
					moveForward(5);
				}

				// Turn 45 degrees left (to face the switch)
				if (counter == 3) {
					turnLeft(45);
				}

				// Raise intake
				if (counter == 4) {
					raiseIntake();
				}

				// Forwards 2 feet
				if (counter == 5) {
					moveForward(2);
				}

				// Shoot cube
				if (counter == 6) {
					shootCube();
				}

				// Kill motors
				if (counter == 7) {
					killMotors();
				}
			} else {
				// Runs if there's an FMS error
				// Forwards 14 feet
				if (counter == 0) {
					moveForward(14);
				}

				// Kill the motors
				if (counter == 1) {
					killMotors();
				}
			}
			break;

		case baselineTime:
			// Fallback for autonomous issues or sensor failure. DO NOT run under most normal circumstances.
			if (counter == 0) {
				// go forward for 3 seconds and cross the baseline
				motorSet(-.5, -.5); 
				Timer.delay(1.75);
				motorSet(0, 0);
				counter++;
			}
			if (counter == 1) {
				motorSet(0, 0);
			}
			break;

		case defaultAuto:
			motorSet(0, 0);
			break;
		}
	}

	@Override
	public void teleopPeriodic() {
		// gets sensor values and displays them in SmartDashboard
		angle = gyro.getAngle();
		irDist = IR.getVoltage();
		pulse = enc1.get();
		dist = enc1.getDistance();

		if (irDist > 1) {
			cube = true;
		} else {
			cube = false;
		}

		SmartDashboard.putNumber("Angle: ", angle);
		SmartDashboard.putNumber("IR Distance: ", irDist);
		SmartDashboard.putNumber("Pulse Count: ", pulse);
		SmartDashboard.putNumber("Distance Traveled: ", dist);
		SmartDashboard.putBoolean("Cube in intake: ", cube);

		// calls drive() to drive
		drive();

		// calls intake() to deal with cube manipulation
		intake();

		// calls switch() to raise the intake to switch height
		moveIntake_Teleop();

		// calls climb() to deploy hook and pull in the winch
		climb();
	}

	/**
	 * Get the proportion of motor speed based on the distance to the target value.
	 * Used in auto.
	 * 
	 * @param target
	 *            the target value
	 * @param currentEnc
	 *            the current encoder value
	 * @return the proportion of the motor speed constant to set
	 */
	public static double prop(double target, double currentEnc) {
		if (((target - currentEnc) / target) > 0.8) {
			return -0.4;
		} else if (((target - currentEnc) / target) < 0.4) {
			return -0.3;
		} else {
			return (((target - currentEnc) / target) * DRIVE_CONSTANT);
		}
	}

	/**
	 * Gets the proportion to be used with motor speed during gyro turns. Used in
	 * auto.
	 * 
	 * Target should ALWAYS be positive.
	 * 
	 * @param target
	 *            the target value
	 * @param currentGyro
	 *            the current gyro value
	 * @return the proportion of the motor speed constant to set
	 */
	public static double propGyro(double target, double currentGyro) {
		if ((1 - (currentGyro / target)) <= 0.5) {
			return 1 - (currentGyro / target);
		}
		return 0.5;
	}

	/**
	 * Turns the robot to the right a specified amount. Auto method.
	 * 
	 * @param targetAngle
	 *            the angle to turn to (in degrees)
	 * @param currentAngle
	 *            the current gyro angle (in degrees)
	 */
	private void turnRight(double targetAngle) {
		// Remember that negative is forwards in auto.
		if (angle > targetAngle) {
			motorSet(0, 0);
			enc1.reset();
			counter++;
		} else {
			System.out.println(propGyro(targetAngle, angle));
			motorSet(-propGyro(targetAngle, angle), propGyro(targetAngle, angle));
		}
	}

	/**
	 * Turns the robot to the left a specified amount. Auto method.
	 * 
	 * @param targetAngle
	 *            the angle to turn to (in degrees)
	 * @param currentAngle
	 *            the current gyro angle (in degrees)
	 */
	private void turnLeft(double targetAngle) {
		// Remember that negative is forwards in auto.
		targetAngle = -targetAngle;
		if (angle < targetAngle) {
			motorSet(0, 0);
			enc1.reset();
			counter++;
		} else {
			System.out.println(propGyro(targetAngle, angle));
			motorSet(propGyro(targetAngle, angle), -propGyro(targetAngle, angle));
			
		}
	}

	/**
	 * Moves the robot forwards a specified distance (in feet). Auto method.
	 * 
	 * @param targetDistance
	 *            the distance to go (feet)
	 * @param currentDistance
	 *            the current distance (feet)
	 */
	private void moveForward(double targetDistance) {
		if (dist >= targetDistance) {
			motorSet(0, 0);
			enc1.reset();
			gyro.reset();
			counter++;
		} else {
			motorSet(prop(targetDistance, dist), prop(targetDistance, dist));
		}
	}

	/**
	 * Shoots out the cube. Auto method.
	 */
	private void shootCube() {
		motorSet(0, 0);
		intakeMotorSet(-1, 1);
		Timer.delay(2);
		intakeMotorSet(0, 0);
		intakeVert.set(0);
		enc1.reset();
		counter++;
	}

	/**
	 * Raises the intake. Auto method.
	 */
	private void raiseIntake() {
		motorSet(0, 0);
		intakeVert.set(-.85);
		Timer.delay(2);
		intakeVert.set(-0.15); // keep the intake from dropping
		counter++;
	}

	/**
	 * Kills the motors. Auto method.
	 */
	private void killMotors() {
		motorSet(0, 0);
		intakeMotorSet(0, 0);
		intakeVert.set(0);
	}

	/**
	 * Used in drive code
	 * 
	 * Sets current positions of joysticks as values to left and right
	 */
	private void drive() {

		if (leftJoy.getRawButton(1) == true && leftJoy.getRawButton(3) == true) {
			scale = 1;
		} else {
			scale = 0.75;
		}

		if (Math.abs(leftJoy.getY()) > 0.2) {
			left = leftJoy.getY() * reverse;
			if (toggle)
				right = leftJoy.getY() * reverse;
		} else {
			left = 0;
		}

		if (toggle) {
			if (Math.abs(rightJoy.getX()) > 0.2) {
				left += rightJoy.getX() * reverse;
				right -= rightJoy.getX() * reverse;
			}
		} else {
			if (Math.abs(rightJoy.getY()) > 0.2) {
				right = rightJoy.getY() * reverse;
			} else {
				right = 0;
			}
		}

		if (left > 1)
			left = 1;
		if (left < -1)
			left = -1;
		if (right > 1)
			right = 1;
		if (right < -1)
			left = -1;

		motorSet(left * scale, right * scale);
	}

	/**
	 * Sets motor speed values
	 * 
	 * @param left
	 *            - left motor speed
	 * @param right
	 *            - right motor speed
	 */
	private void motorSet(double left, double right) {
		frontLeft.set(left);
		backLeft.set(left);

		frontRight.set(-right);
		backRight.set(-right);
	}

	/**
	 * Activates or deactivates the intake based on the left stick y-axis input.
	 */
	private void intake() {
		if (Math.abs(xbox.getRawAxis(1)) > 0.2) {
			intakeL = xbox.getRawAxis(1);

		} else {
			intakeL = 0;
		}

		if (Math.abs(xbox.getRawAxis(5)) > 0.2) {
			intakeR = xbox.getRawAxis(5);
		} else {
			intakeR = 0;
		}

		intakeMotorSet(intakeL, -intakeR);
	}

	/**
	 * Raises and lowers the intake to access the switch to score during teleop
	 * 
	 * A button raises, B button lowers
	 */
	private void moveIntake_Teleop() {
		// Goes up
		if (xbox.getRawAxis(2) > 0.7) {
			height = true;
			intakeVert.set(xbox.getRawAxis(2));
		}
		// Goes down
		else if (xbox.getRawButton(5) == true) {
			height = false;
			intakeVert.set(-.25);
		}
		// Maintains height
		else if (height) {
			intakeVert.set(.15);
		}
		// Stops motors if not needed
		else {
			intakeVert.set(0);
		}
	}

	/**
	 * Sets intake motor speed
	 * 
	 * @param i1
	 *            intake 1 motor speed
	 * @param i2
	 *            intake 2 motor speed
	 */
	private void intakeMotorSet(double i1, double i2) {
		intake1.set(i1);
		intake2.set(i2);
	}

	/*
	 * Sets hook & winch motor speed
	 */
	private void climb() {
		if (xbox.getXButton() == true) {
			hooker = 0.5;
		} else if (xbox.getAButton() == true) {
			hooker = -.75;
		} else {
			hooker = 0;
		}

		if (xbox.getRawAxis(3) > 0.7) {
			wench = xbox.getRawAxis(3);
		} else if (xbox.getRawButton(6) == true) {

		} else {
			wench = 0;
		}

		hook.set(hooker);
		winch.set(wench);
	}

}
